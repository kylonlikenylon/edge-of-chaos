﻿using UnityEngine;
using EasyButtons;
using System;

public class EoC2D : MonoBehaviour
{
    /* +++++ SETUP +++++ */
    [Header("Setup")]
    [Range(2, 100)]
    public int maxStates = 2;
    [Range(8, 2048)]
    public int rez = 8;

    [Range(0, 50)]
    public int stepsPerFrame = 0;

    [Range(1, 50)]
    public int stepMod = 1;


    public ComputeShader cs;

    private int step;
    public Material outMat;
    private RenderTexture outTex;

    private int stepKernel;

    private ComputeBuffer buffer;
    private int totalStates;

    void Update()
    {
        if (Time.frameCount % stepMod == 0)
        {
            for (int i = 0; i < stepsPerFrame; i++)
            {
                Step();
            }
        }
    }

    [Button]
    public void Step()
    {
        cs.SetInt("step", step);
        cs.SetInt("rez", rez);
        cs.SetInt("maxstates", maxStates);
        cs.SetBuffer(stepKernel, "buffer", buffer);
        cs.SetTexture(stepKernel, "outTex", outTex);

        cs.Dispatch(stepKernel, rez / 8, rez, 1);

        outMat.SetTexture("_UnlitColorMap", outTex);
        step = step < rez ? step + 1 : step;
    }

    void Start()
    {
        Reset();
    }

    void OnDestroy()
    {
        buffer.Release();
    }

    [Button]
    private void Reset()
    {
        Initiate();
        CreateBuffer();
        GPUResetKernel();
    }

    private void Initiate()
    {
        outTex = CreateTexture(RenderTextureFormat.ARGBFloat);

        stepKernel = cs.FindKernel("StepKernel");

    }

    private void CreateBuffer()
    {
        int totalStates = maxStates * maxStates * maxStates;
        buffer = new ComputeBuffer(totalStates, sizeof(int));
        System.Random generator = new System.Random();

        int[] initial = new int[totalStates];
        for (int i = 0; i < totalStates; i++)
        {
            initial[i] = generator.Next(0, maxStates);
            // Debug.Log($"{i}: {initial[i]}");
        }

        buffer.SetData(initial);
    }

    protected RenderTexture CreateTexture(RenderTextureFormat format)
    {
        RenderTexture texture = new RenderTexture(rez, rez, 1, format);
        texture.enableRandomWrite = true;
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Repeat;
        texture.useMipMap = false;
        texture.Create();
        return texture;
    }

    private void GPUResetKernel()
    {
        step = 1;
        int resetKernel = cs.FindKernel("ResetKernel");
        cs.SetTexture(resetKernel, "outTex", outTex);

        DateTime now = DateTime.Now;
        cs.SetInt("time", now.Millisecond);

        cs.Dispatch(resetKernel, rez, rez, 1);

        outMat.SetTexture("_UnlitColorMap", outTex);
    }
}
